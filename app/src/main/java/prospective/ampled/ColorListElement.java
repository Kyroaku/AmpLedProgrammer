package prospective.ampled;

/**
 * Created by Marcin Dziedzic on 2018-05-22.
 */

public class ColorListElement {

    int r, g, b;

    public ColorListElement() {
        r = g = b = -1;
    }

    public boolean IsItAddButton() {
        if(r < 0 || g < 0 || b < 0)
            return true;
        else
            return false;
    }

    @Override
    public String toString() {
        if(IsItAddButton())
            return "+";
        else
            return "";
    }
}
