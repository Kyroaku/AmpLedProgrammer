package prospective.ampled;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

/**
 * Created by Marcin Dziedzic on 2018-05-26.
 */

public class ColorListAdapter extends ArrayAdapter<ColorListElement> {
    ColorListAdapter(Context context, int resource) {
        super(context, resource);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = super.getView(position, convertView, parent);
        ColorListElement element = getItem(position);
        view.setBackgroundColor(Color.rgb(element.r, element.g, element.b));
        return view;
    }
}
