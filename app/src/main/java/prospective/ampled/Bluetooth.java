package prospective.ampled;

/**
 * Created by Marcin Dziedzic on 2017-11-06.
 */

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;

/**
 * Bluetooth manager helper class.
 */
class Bluetooth
{
    private BluetoothAdapter mAdapter;
    private Handler handler;
    private Context context;

    /**
     * Constructor.
     * @param context Context of the activity.
     */
    Bluetooth(Context context) {
        this.context = context;
        handler = new Handler(context.getMainLooper());

        /* Get default bluetooth adapter. */
        mAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mAdapter == null) {
            toast("No bluetooth adapter");
            Log.e("Bluetooth", "No bluetooth adapter");
        } else if (!mAdapter.isEnabled()) {
            toast("Bluetooth dissabled");
            Log.e("Bluetooth", "Bluetooth disabled");
        }
    }

    /**
     * Listens for the connection with client and returns socket of the connected device.
     * @return Socket of the accepted client or null in case of error.
     */
    public BluetoothSocket Accept()
    {
        /* Start listening for client with specific UUID. */
        BluetoothServerSocket server = null;
        try {
            server = mAdapter.listenUsingRfcommWithServiceRecord(
                    context.getString(R.string.app_name),
                    UUID.fromString("b2b097ed-d105-4cb6-8d03-4809d7efeda4")
            );
        } catch (IOException e) {
            Log.e("Bluetooth", "Server error: " + e.getMessage());
        }

        /* Wait for client to accept. */
        BluetoothSocket socket = null;
        try {
            socket = server.accept();
        }
        catch (IOException e) {
            Log.e("Bluetooth", "Accept error: " + e.getMessage());
        }
        catch (NullPointerException e) {
            e.printStackTrace();
        }

        /* Close server. */
        try {
            server.close();
        } catch (IOException e) {
            Log.e("Bluetooth", "Close error: " + e.getMessage());
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        return socket;
    }

    /**
     * Connects to device with specified name.
     * Device has to be paired first.
     * @param name Name of the bluetooth device.
     * @return Socket of the connected device or null in case of error.
     */
    BluetoothSocket Connect(String name)
    {
        BluetoothSocket socket = null;
        BluetoothDevice device = null;

        /* Get list of paired devices. */
        Set<BluetoothDevice> devices = mAdapter.getBondedDevices();

        /* Find device with specified name. */
        for(BluetoothDevice d : devices) {
            if (d.getName().equals(name)) {
                device = d;
                break;
            }
        }

        if(device == null) {
            toast("No device named '"+name+"'.");
            Log.i("Bluetooth", "No device named '"+name+"'");
            return null;
        }
        /* Connect with device. */
        try {
            socket = device.createRfcommSocketToServiceRecord(
                    //UUID.fromString("b2b097ed-d105-4cb6-8d03-4809d7efeda4")
                    UUID.fromString("00001101-0000-1000-8000-00805F9B34FB")
            );
            socket.connect();
        }
        catch (IOException e) {
            Log.e("Bluetooth", "Connect error: " + e.getMessage());
            toast("Connect error.");
            try {
                socket.close();
            } catch (IOException e2) {
                Log.e("Bluetooth", "Close error: " + e2.getMessage());
            } catch (NullPointerException e2) {
                e2.printStackTrace();
            }
            socket = null;
        }
        return socket;
    }

    /**
     * Sends byte array to the device specified by socket.
     * @param socket Socket of the device to which data will be sent.
     * @param data Data to send.
     */
    void Send(BluetoothSocket socket, byte[] data)
    {
        OutputStream stream;
        try
        {
            stream = socket.getOutputStream();
            stream.write(data);
        }
        catch (IOException e) {
            Log.i("Bluetooth", "Send error: " + e.getMessage());
        }
    }

    /**
     * Receives data from the device specified by socket.
     * @param socket Socket of the device from which data will be received.
     * @param data Buffer for received data.
     */
    public void Recv(BluetoothSocket socket, byte[] data)
    {
        InputStream stream;
        try
        {
            stream = socket.getInputStream();
            stream.read(data);

        }
        catch (IOException e) {
            Log.i("Bluetooth", "Receive error: " + e.getMessage());
        }
    }

    /**
     * Helper method for showing toasts.
     * @param text Text to show in toast.
     */
    private void toast(final String text)
    {
        handler.post(new Runnable() {
            public void run()
            {
                Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
