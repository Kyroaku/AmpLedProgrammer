package prospective.ampled;

/**
 * Created by Marcin Dziedzic on 2018-04-01.
 */

class ListElement {

    String mName;
    int mValue;

    ListElement(String name, int value) {
        mName = name;
        mValue = value;
    }

    @Override
    public String toString() {
        return mName;
    }
}
