package prospective.ampled;

/**
 * Created by Marcin Dziedzic on 2018-04-01.
 */

class Frame {

    static final byte SetAnimationCommand = 0;
    static final byte SetColorsCommand = 1;
    static final byte SetSpeedCommand = 2;
    static final byte SetLedsNumberCommand = 3;
    static final byte SetFoftnessCommand = 4;
    static final byte SetSizeCommand = 5;
    static final byte SetAllCommand = 6;

    static byte[] Build(int command, int data) {
        /* Build frame. */
        byte[] frame = new byte[] {
                (byte)0xFF, 2, (byte)command, (byte)data, 0
        };
        /* Calculate checksum. */
        for(int i = 1; i < frame.length-1; i++)
            frame[frame.length-1] += frame[i];
        return frame;
    }

    static byte[] Build(int command, int[] data) {
        /* Build frame. */
        byte[] frame = new byte[data.length + 4];
        frame[0] = (byte)0xFF;
        frame[1] = (byte)(data.length + 1);
        frame[2] = (byte)command;
        for(int i = 0; i < data.length; i++)
            frame[i+3] = (byte)data[i];
        /* Calculate checksum. */
        for(int i = 1; i < frame.length-1; i++)
            frame[frame.length-1] += frame[i];
        return frame;
    }

    static byte[] BuildSetAllCommand(int animation, int speed, int softness, int size) {
        return Build(SetAllCommand, new int[] { animation, speed, softness, size });
    }

    static byte[] BuildSetColorsCommand(int ledsNumber, int[] colors) {
        int[] params = new int[3*ledsNumber + 1];
        params[0] = ledsNumber;
        for(int i = 0; i < colors.length; i++) {
            params[i+1] = colors[i];
        }
        return Build(SetColorsCommand, params);
    }
}
