package prospective.ampled;

import android.bluetooth.BluetoothSocket;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Switch;

class MainActivity extends AppCompatActivity {
    Spinner mAnimationSpinner;
    SeekBar mSpeedSeekBar, mSoftnessSeekBar, mSizeSeekBar;
    Button mProgramButton;
    Switch mConnectionSwitch;
    ProgressBar mConnectionProgress;
    ListView mColorsList;

    Button mTestButton;

    Bluetooth mBluetooth;                       /**< Bluetooth manager. */
    BluetoothSocket mBluetoothClient = null;    /**< Bluetooth client. */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        mBluetooth = new Bluetooth(this);

        /* Create listeners. */
        OnClickListener clickListener = new OnClickListener();
        OnItemClickListener itemClickListener = new OnItemClickListener();

        /* Find widgets. */
        mAnimationSpinner = (Spinner)findViewById(R.id.animation_spinner);
        mSpeedSeekBar = (SeekBar)findViewById(R.id.speed_seekbar);
        mSoftnessSeekBar = (SeekBar)findViewById(R.id.softness_seekbar);
        mSizeSeekBar = (SeekBar)findViewById(R.id.size_seekbar);
        mProgramButton = (Button)findViewById(R.id.program_button);
        mConnectionSwitch = (Switch)findViewById(R.id.connection_switch);
        mConnectionProgress = (ProgressBar)findViewById(R.id.connection_progress);
        mColorsList = (ListView)findViewById(R.id.colors_list);

        /* Init animation spinner. */
        ArrayAdapter<ListElement> animationsAdapter = new ArrayAdapter<>(
                this, R.layout.support_simple_spinner_dropdown_item
        );
        animationsAdapter.add(new ListElement("Ping Pong", 0));
        animationsAdapter.add(new ListElement("Double Ping Pong", 1));
        animationsAdapter.add(new ListElement("Fade", 2));
        animationsAdapter.add(new ListElement("Soft Switch", 3));
        animationsAdapter.add(new ListElement("Static Color", 4));
        animationsAdapter.add(new ListElement("Running Two", 5));
        animationsAdapter.add(new ListElement("Particles", 6));
        animationsAdapter.add(new ListElement("Particles Dark", 7));
        mAnimationSpinner.setAdapter(animationsAdapter);

        /* Init connection progress bar. */
        mConnectionProgress.setVisibility(ProgressBar.INVISIBLE);

        /* Init colors list. */
        ColorListAdapter colorsAdapter = new ColorListAdapter(
                this, R.layout.color_list_element
        );
        colorsAdapter.add(new ColorListElement());
        mColorsList.setAdapter(colorsAdapter);

        /* Set listeners. */
        mProgramButton.setOnClickListener(clickListener);
        mConnectionSwitch.setOnClickListener(clickListener);
        mColorsList.setOnItemClickListener(itemClickListener);

        ConnectToDriver();
    }

    @Override
    protected void onPause() {
        super.onPause();

        DisconnectDriver();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        DisconnectDriver();
    }

    @Override
    protected void onStop() {
        super.onStop();

        DisconnectDriver();
    }

    /**
     * Callback class for every click events.
     */
    private class OnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            switch(v.getId()) {
                /* Program button clicked. */
                case R.id.program_button:
                    ProgramDriver();
                    break;

                /* Connection switch clicked. */
                case R.id.connection_switch:
                    if(mConnectionSwitch.isChecked()) {
                        ConnectToDriver();
                    } else {
                        DisconnectDriver();
                    }
                    break;
            }
        }
    }

    /**
     * Callback class for clicking items on list.
     */
    private class OnItemClickListener implements AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final int index = position;
            final ColorListElement element = (ColorListElement)mColorsList.getAdapter().getItem(index);

            if(element.IsItAddButton()) {
                /* Set element to be color instead "add" button. */
                element.r = element.g = element.b = 0;
                /* Add new color to the list. */
                ArrayAdapter<ColorListElement> adapter = (ArrayAdapter<ColorListElement>)mColorsList.getAdapter();
                adapter.add(new ColorListElement());
            }

            ColorPickerDialogFragment dialog = new ColorPickerDialogFragment();
            /* Set initial color values. */
            dialog.SetColors(element.r, element.g, element.b);
            dialog.SetOnColorPickListener(new ColorPickerDialogFragment.OnColorPickListener() {
                @Override
                public void OnColorPick(int red, int green, int blue) {
                    /* Set picked color. */
                    element.r = red;
                    element.g = green;
                    element.b = blue;
                    /* Update views. */
                    mColorsList.invalidateViews();
                }
            });
            dialog.show(getFragmentManager(), "color picker");
        }
    }

    /**
     * Sends chosen settings to driver.
     */
    private void ProgramDriver() {
        if(mBluetoothClient == null)
            return;
        if(!mBluetoothClient.isConnected())
            return;

        /* Program settings. */
        ListElement element = (ListElement)mAnimationSpinner.getSelectedItem();
        final byte[] setAllFrame = Frame.BuildSetAllCommand(
                element.mValue,
                mSpeedSeekBar.getProgress(),
                mSoftnessSeekBar.getProgress(),
                mSizeSeekBar.getProgress()
        );

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                mBluetooth.Send(mBluetoothClient, setAllFrame);
            }
        });
        thread.start();
        try {
            thread.join();
        } catch(InterruptedException e) {
            e.printStackTrace();
        }

        /* Program colors. */
        int numColors = mColorsList.getAdapter().getCount()-1;
        if(numColors <= 0)
            return;

        int[] colors = new int[numColors*3];
        for(int i = 0; i < numColors; i++) {
            ColorListElement color = (ColorListElement)mColorsList.getAdapter().getItem(i);
            colors[i*3+0] = color.r;
            colors[i*3+1] = color.g;
            colors[i*3+2] = color.b;
        }
        final byte[] setColorsFrame = Frame.BuildSetColorsCommand(numColors, colors);
        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                mBluetooth.Send(mBluetoothClient, setColorsFrame);
            }
        });
        thread.start();
        try {
            thread.join();
        } catch(InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Updates UI widgets that are dependent on connection state.
     * Should be called after connection action has been finished.
     */
    private void UpdateUIConnectionState() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                /* Hide progress bar. */
                mConnectionProgress.setVisibility(ProgressBar.INVISIBLE);
                /* Update program button state. */
                if(mBluetoothClient != null) {
                    mProgramButton.setEnabled(true);
                    mConnectionSwitch.setChecked(true);
                }
                else {
                    mProgramButton.setEnabled(false);
                    mConnectionSwitch.setChecked(false);
                }

                /* Enable connection switch. */
                mConnectionSwitch.setEnabled(true);
            }
        });
    }

    /**
     * Connects with driver's Bluetooth module.
     * Should be called on UI thread. While connecting, progress bar is shown.
     */
    private void ConnectToDriver() {
        /* Disable connection switch while connecting. */
        mConnectionSwitch.setEnabled(false);
        /* Show progress bar. */
        mConnectionProgress.setVisibility(ProgressBar.VISIBLE);

        new Thread(new Runnable() {
            @Override
            public void run() {
                /* Connect. */
                mBluetoothClient = mBluetooth.Connect("Sasha");

                try {
                    Thread.sleep(200);
                } catch(Exception e) {
                    e.printStackTrace();
                }

                UpdateUIConnectionState();
            }
        }).start();
    }

    /**
     * Disconnects driver's bluetooth module.
     */
    private void DisconnectDriver() {
        /* Disable connection switch while disconnecting. */
        mConnectionSwitch.setEnabled(false);
        /* Show progress bar. */
        mConnectionProgress.setVisibility(ProgressBar.VISIBLE);

        new Thread(new Runnable() {
            @Override
            public void run() {
                /* Disconnect. */
                try {
                    mBluetoothClient.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    Thread.sleep(1000);
                } catch(Exception e) {
                    e.printStackTrace();
                }

                UpdateUIConnectionState();
            }
        }).start();
    }
}