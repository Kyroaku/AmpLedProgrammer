package prospective.ampled;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

/**
 * Created by Marcin Dziedzic on 2018-05-15.
 */

public class ColorPickerDialogFragment extends DialogFragment {
    OnColorPickListener mOnColorPickListener;
    SeekBar mRedSeekBar, mGreenSeekBar, mBlueSeekBar;
    TextView mColorView;
    int mRed, mGreen, mBlue;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        OnSeekBarChangeListenerImpl seekBarChangeListener = new OnSeekBarChangeListenerImpl();

        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.color_picker, null);
        mRedSeekBar = (SeekBar)view.findViewById(R.id.red_seekbar);
        mGreenSeekBar = (SeekBar)view.findViewById(R.id.green_seekbar);
        mBlueSeekBar = (SeekBar)view.findViewById(R.id.blue_seekbar);
        mColorView = (TextView)view.findViewById(R.id.color_view);

        mRedSeekBar.setOnSeekBarChangeListener(seekBarChangeListener);
        mGreenSeekBar.setOnSeekBarChangeListener(seekBarChangeListener);
        mBlueSeekBar.setOnSeekBarChangeListener(seekBarChangeListener);

        mRedSeekBar.setProgress(mRed);
        mGreenSeekBar.setProgress(mGreen);
        mBlueSeekBar.setProgress(mBlue);

        builder.setTitle("Pick color")
                .setView(view)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mOnColorPickListener.OnColorPick(
                                mRedSeekBar.getProgress(),
                                mGreenSeekBar.getProgress(),
                                mBlueSeekBar.getProgress()
                        );
                    }
                });
        return builder.create();
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);

        mOnColorPickListener.OnColorPick(
                mRedSeekBar.getProgress(),
                mGreenSeekBar.getProgress(),
                mBlueSeekBar.getProgress()
        );
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);

        mOnColorPickListener.OnColorPick(
                mRedSeekBar.getProgress(),
                mGreenSeekBar.getProgress(),
                mBlueSeekBar.getProgress()
        );
    }

    public void SetColors(int r, int g, int b) {
        mRed = r;
        mGreen = g;
        mBlue = b;
    }

    public void SetOnColorPickListener(OnColorPickListener listener) {
        mOnColorPickListener = listener;
    }

    public interface OnColorPickListener {
        void OnColorPick(int red, int green, int blue);
    }

    private class OnSeekBarChangeListenerImpl implements SeekBar.OnSeekBarChangeListener {

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            mColorView.setBackgroundColor(Color.rgb(
                    mRedSeekBar.getProgress(),
                    mGreenSeekBar.getProgress(),
                    mBlueSeekBar.getProgress()
            ));
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    }
}
